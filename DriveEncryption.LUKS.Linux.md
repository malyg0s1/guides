# How to set up Drive Encryption with LUKS2 on Linux

## Prepare

0. `# apt update && apt install cryptsetup`

1. `# fdisk -l`
    - so you know on which device (drive or partition) you want to set up LUKS

## Setup drive

1. `# cryptsetup luksFormat -y -v --type luks2 <device> --cipher aes-xts-plain64`
    - `<device>` : either a drive (`/dev/sdc`), or a partition (`/dev/sdc1`)
    - `-v`: verbose
    - `-y`: verify passprase again
    - `--type luks2`: use luks2 (KDF:argon2id, enables integrity check)
    - EXPERIMENTAL (cryptsetup 2.6.1-git) `--integrity`
        - `--cipher aes-xts-plain64 --integrity hmac-sha256`
        - `--cipher chacha20-random --integrity poly1305`
    - `--label SUPER_SECRET_DRIVE`: set label
    - This command initializes the volume, and sets an initial key or passphrase.
      Note that the passphrase is not recoverable.

2. Verify
    - `# cryptsetup luksOpen /dev/<device> <mapping_name>`
        - open device
    - `# cryptsetup -v status <mapping_name>`
    - `# ls -l /dev/mapper/<mapping_name>`
    - `# cryptsetup luksDump /dev/<device>`
        - dump headers

## Format partition

0. Zero out the drive
    - `pv -tpreb /dev/zero | dd of=/dev/mapper/<mapping_name> bs=128M`
    - or
    - `dd if=/dev/zero of=/dev/mapper/<mapping_name> bs=128M status=progress`

1. Format partition
    - `mkfs.ext4 /dev/mapper/<mapping_name>`
   
2. Verify
    - `mkdir /mnt/<mount_name>`
    - `mount /dev/mapper/<mapper_name> /mnt/<mount_name>`
    - `df -H`

3. Set default permissions
    - `cd /dev/<mount_name>`
    - `mkdir <drive_root>`
    - `setfacl -m u::rwx,g::rwx,o::rwx,m::rwx <drive_root>/`
    - `setfacl -d -m u::rwx,g::rwx,o::rwx,m::rwx <drive_root>/`
        - these are the default permissions

## Usage

- Basic usage
    - `cryptsetup luksOpen /dev/<device> <mapping_name>`
    - `mount /dev/mapper/<mapper_name> /mnt/<mount_name>`
    - ...
    - `umount /mev/<mount_name>`
    - `cryptsetup luksClose <mapping_name>`

- How can I run *fcsk* on the partition?
    - `umount /mnt/<mount_name>`
    - `fsck -vy /dev/mapper/<mapping_name>`

- How can I change the password?
    - see key slots, max -8 i.e. max 8 passwords can be setup for each device
    - `cryptsetup luksDump /dev/<device>`
    - `cryptsetup luksAddKey /dev/<device>`
        - Add new key
    - `cryptsetup luksRemoveKey /dev/<device>`
        - Delete old key
